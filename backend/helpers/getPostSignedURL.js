var AWS = require('aws-sdk');

var s3 = new AWS.S3();

var params = {
    Bucket: process.env.S3_BUCKET,
    Key: null,
    Expires: 60,
    ContentType: 'image/jpeg'
};
exports.generatePresignedURL = function (imageUID) {
    params.Key = imageUID; // Here we ensure that all the search faces are in the same directory
    return new Promise((resolve, reject) => {
        s3.getSignedUrl('putObject', params, function (err, data) {
            if (err) {
                console.log('An error occurred when trying to get a signed s3 URL.', err)
                reject(err)
            } else {
                console.log('I am resolvin the response...', data)
                resolve(data)
            }
        })
    })
};