"use strict";
var lexResponses = require("../lexResponses");
var databaseHelper = require("../../databaseHelper");
var S3Helper = require("../../s3Helper");

function capitalizeFirstWord (string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports = async function (intentRequest) {
  var selectedCountry = intentRequest.currentIntent.slots.country.toLowerCase();
  var userId = intentRequest.userId;
  console.log("The selected country is: ", selectedCountry);

  try {
    let stateCards = await databaseHelper.scanTableForCountry(selectedCountry);
    // console.log('Here the response from scan perdidos: ', stateCards)
    var allCards = [];
    if (stateCards.Items.length > 0) {
      for (var i = 0; i < stateCards.Items.length; i++) {
        const s3ImageHelper = new S3Helper(stateCards.Items[i].imageS3Key, "perdidosMedia/");
        let signedS3ImageUrl = await s3ImageHelper.signedS3GetUrl();
        const s3PDFHelper = new S3Helper(stateCards.Items[i].pdfS3Key, "perdidosMedia/");
        let signedS3PDFUrl = await s3PDFHelper.signedS3GetUrl();
        let card = { title: `Name: ${stateCards.Items[i].name}. Country: ${capitalizeFirstWord(stateCards.Items[i].country)}`, 
                     subTitle: `Age: ${stateCards.Items[i].age}. Report Date: ${stateCards.Items[i].happened}.`, 
                     imageUrl: signedS3ImageUrl, 
                     attachmentLinkUrl: signedS3PDFUrl };
        // console.log("Pushing a new card for response: ", card);
        allCards.push(card);
      }
    } else {
      console.log("No cards for that state: ", selectedCountry);
      return new Promise(resolve => {
        // console.log('This is the answer from the Intelligence Provider: ', res)
        let fullfilledQuery = { fullfilmentState: "Fulfilled", message: { contentType: "PlainText", content: `We do not have records for ${capitalizeFirstWord(selectedCountry)}, at this point we only have reports for Mexico` } };
        if (!intentRequest.sessionAttributes) {
          intentRequest.sessionAttributes = {};
        }
        intentRequest.sessionAttributes["selectedCountry"] = selectedCountry; // Here we add the session attributes
        // intentRequest.sessionAttributes["allCards"] = allCards; // Here we remove the cards from last section
        intentRequest.sessionAttributes["totalCards"] = stateCards.Count; // Here we remove the cards from last secti
        const lexRes = lexResponses.close(intentRequest.sessionAttributes, fullfilledQuery.fullfilmentState, fullfilledQuery.message, allCards);
        resolve(lexRes);
      });
    }

    // Here we return the information
    return new Promise(resolve => {
      // console.log('This is the answer from the Intelligence Provider: ', res)
      let fullfilledQuery = { fullfilmentState: "Fulfilled", message: { contentType: "PlainText", content: `For ${capitalizeFirstWord(selectedCountry)}, we have ${allCards.length} reports` } };
      if (!intentRequest.sessionAttributes) {
        intentRequest.sessionAttributes = {};
      }
      intentRequest.sessionAttributes["selectedCountry"] = selectedCountry; // Here we add the session attributes
      // intentRequest.sessionAttributes["allCards"] = allCards; // Here we remove the cards from last section
      intentRequest.sessionAttributes["totalCards"] = stateCards.Count; // Here we remove the cards from last secti
      const lexRes = lexResponses.close(intentRequest.sessionAttributes, fullfilledQuery.fullfilmentState, fullfilledQuery.message, allCards);
      resolve(lexRes);
    });

  } catch (error) {
    console.log("An error ocurred retrieving all the cards for: ", selectedCountry, error);
  }

};
