"use strict";

const lexResponses = require("../lexResponses");
const databaseManager = require("../../databaseHelper");

async function validateSelectedCountry (selectedCountry) {
    let stateCards = await databaseHelper.scanTableForselectedCountry(selectedselectedCountry);
    // console.log('Here the response from scan perdidos: ', stateCards)
    if (stateCards.Items.length > 0) {
        return true
    } else {
        return false
    }
}   

module.exports = async function (intentRequest) {
    var selectedCountry = intentRequest.currentIntent.slots.country;
    var userId = intentRequest.userId;
    const slots = intentRequest.currentIntent.slots;

    if (selectedCountry === null) {
        return lexResponses.delegate(intentRequest.sessionAttributes, intentRequest.currentIntent.slots);
    } else {
        try {
            selectedCountry = selectedCountry.toLowerCase();
            const countryAvailable = await validateSelectedCountry(selectedCountry);
            if (countryAvailable) {
                return lexResponses.delegate(intentRequest.sessionAttributes, slots);
            } else {
                return lexResponses.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, selectedCountry, { contentType: 'PlainText', content: 'At this moment, we only have records for Mexico' });
            }
        } catch (error) {
            console.log('An error ocurred validating the chatbot slots: ', error)
            return lexResponses.delegate(intentRequest.sessionAttributes, slots);
        }
    }
}
