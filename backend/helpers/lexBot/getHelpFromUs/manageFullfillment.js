"use strict";
var lexResponses = require("../lexResponses");
// var databaseHelper = require("../../databaseHelper");
// var S3Helper = require("../../s3Helper");

const AWS = require("aws-sdk");
AWS.config.update({ region: "us-east-1" });

var sns = new AWS.SNS();
var sns_topic = process.env.SNS_TOPIC_ARN

module.exports = async function (intentRequest) {
    var email = intentRequest.currentIntent.slots.email;
    var age = intentRequest.currentIntent.slots.age;
    var year = intentRequest.currentIntent.slots.year;
    var name = intentRequest.currentIntent.slots.name;
    var last_name = intentRequest.currentIntent.slots.last_name;
    var phone = intentRequest.currentIntent.slots.phone;
    var userId = intentRequest.userId;

    console.log("The user requesting a report is: ", email);

    var params = {
        Message: `From ${email}. ${name} ${last_name} is lost. She is ${age} years old. We do not see him/her since ${year}. My phone is ${phone}`,
        Subject: `New Second Alert Report Request from ${email} | ${userId}`,
        TopicArn: sns_topic
    }
    let snsResponse = await sns.publish(params).promise();
    console.log('Here the SNS topic response: ', snsResponse)
    return new Promise(resolve => {
        // console.log('This is the answer from the Intelligence Provider: ', res)
        let fullfilledQuery = { fullfilmentState: "Fulfilled", message: { contentType: "PlainText", content: `Thank your for your request. We will contact you in order to add his/her face to our platform.` } };
        if (!intentRequest.sessionAttributes) {
            intentRequest.sessionAttributes = {};
        }
        const lexRes = lexResponses.close(intentRequest.sessionAttributes, fullfilledQuery.fullfilmentState, fullfilledQuery.message, []);
        resolve(lexRes);
    });
}
