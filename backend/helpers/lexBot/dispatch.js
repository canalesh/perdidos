'use strict';

const getPerdidosInformation = require('./getPerdidosInformation/getPerdidosInformation');
const getHelpFromUs = require("./getHelpFromUs/getHelpFromUs");

module.exports = function (intentRequest) {
    console.log(`dispatch userId=${intentRequest.userId}, intentName=${intentRequest.currentIntent.name}`);
    const intentName = intentRequest.currentIntent.name;

    console.log(intentName + " was called");
    if (intentName === "GetPerdidosInformation") {
        return getPerdidosInformation(intentRequest);
    }

    console.log(intentName + ' was called');
    if (intentName === "GetHelpFromUs") {
      return getHelpFromUs(intentRequest);
    }

    throw new Error(`Intent with name ${intentName} not supported`);
};
