'use strict';

module.exports.delegate = function (sessionAttributes, slots) {
    return {
        sessionAttributes,
        dialogAction: {
            type: 'Delegate',
            slots
        }
    };
};

module.exports.elicitSlotWithCards = function (sessionAttributes, intentName, slots, slotToElicit, message, title, imageUrl, buttons) {
    return {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,
            responseCard: getResponseCards(title, imageUrl, buttons)
        }
    };
};

module.exports.elicitSlot = function (sessionAttributes, intentName, slots, slotToElicit, message) {
    return {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message
        }
    };
};

module.exports.close = function (sessionAttributes, fulfillmentState, message, cardsArrayOfObjects) {
    if (cardsArrayOfObjects.length > 0) {
        return {
            sessionAttributes,
            dialogAction: {
                type: 'Close',
                fulfillmentState,
                message,
                responseCard: getResponseCards(cardsArrayOfObjects)
            }
        };
    } else {
        return {
            sessionAttributes,
            dialogAction: {
                type: 'Close',
                fulfillmentState,
                message
            }
        };
    }
};

module.exports.confirmIntent = function (sessionAttributes, intentName, slots, message) {
    return {
        sessionAttributes,
        dialogAction: {
            type: 'ConfirmIntent',
            intentName,
            slots,
            message
        }
    };
};

function getResponseCards(cardsArrayOfObjects) {
    return {
        contentType: 'application/vnd.amazonaws.card.generic',
        genericAttachments: cardsArrayOfObjects.map(val => {
            return { title: val.title, subTitle: val.subTitle, imageUrl: val.imageUrl, attachmentLinkUrl: val.attachmentLinkUrl };
        }).slice(0, 10) // At this moment, here we limit it to 10 results
    };
}
