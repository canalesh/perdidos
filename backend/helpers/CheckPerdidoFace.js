'use strict';

const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

const rekognition = new AWS.Rekognition({ apiVersion: '2016-06-27' });
var databaseHelper = require("./databaseHelper");
var S3Helper = require("./s3Helper");

class CheckPerdidoFace {

    constructor (s3Config) {
        this.imageName = s3Config.imageName;
    }

    async searchFacesOnImages() {
        try {
            // console.log('TO test, upload the face that is being tested: ', detectedFaces[idx].Blob)
            // await this.putS3Object(this.imageName +'childBlob.jpg', detectedFaces[idx].Blob)
            var params = {
                CollectionId: process.env.FACES_COLLECTION,
                FaceMatchThreshold: 75,
                // Image: {
                //     Bytes: detectedFaces[idx].Blob
                // },
                Image: {
                    S3Object: {
                        Bucket: process.env.S3_BUCKET,
                        Name: this.imageName,
                    }
                },
                MaxFaces: 1
            };
            let faceIdObjects = await rekognition.searchFacesByImage(params).promise() // data.FaceMatches
            // console.log("Here the response from search faces: ", faceIdObjects.FaceMatches[0].Face.FaceId);
            if (faceIdObjects.FaceMatches.length > 0) {
              const faceId = faceIdObjects.FaceMatches[0].Face.FaceId || undefined;
              return faceId;
            } else {
              return false;
            }
        } catch (error) {
            console.log('An error occurred when searching faces: ', error)
            return 'error'
        }
    }

    async main () {
        console.log('Here the input data: ', this.imageName)
        let faceId = await this.searchFacesOnImages()
        if (faceId === 'error') {
            return { perdidoData: null, errorMessage: 'Please, take the picture once again. Your face should appear clear and complete.' }
        } else if (faceId) {
            let databaseResponse = await databaseHelper.scanTableForFaceId(faceId);
            // console.log("Here the database response: ", databaseResponse);
            if (databaseResponse.Items.length > 0) {
                let perdidoData = databaseResponse.Items[0] // Here we just get the database response
                const s3ImageHelper = new S3Helper(perdidoData.imageS3Key, "perdidosMedia/"); 
                perdidoData["imageUrl"] = await s3ImageHelper.signedS3GetUrl(); // Here we generate an URL for the image
                const s3PDFHelper = new S3Helper(perdidoData.pdfS3Key, "perdidosMedia/");
                perdidoData["pdfUrl"] = await s3PDFHelper.signedS3GetUrl(); // Here we generate an URL for the pdf
                return { perdidoData: perdidoData };
            } else {
                return { perdidoData: null, errorMessage: `We do not have records for your face. 
                                                           If you need help, go to the closest police station.` };
            }
        } else {
            return { perdidoData: null, errorMessage: `We do not have records for your face. 
                                                       If you need help, go to the closest police station.`  }
        }
    }

}

// databaseHelper.scanTableForFaceId('03641eb8-d8ab-4634-bd50-92958962780').then(res => console.log(res))

// const checkPerdidoFace = new CheckPerdidoFace({ imageName: 'perdidosMedia/880_Aguascalientes.jpg'})
// checkPerdidoFace.main().then(res => console.log('Here the class response: ', res))

module.exports = CheckPerdidoFace;
