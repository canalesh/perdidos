'use strict';

const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

const S3 = new AWS.S3();
class S3Helper {
    constructor(objectKey, folder) {
        this.objectKey = objectKey;
        this.folder = folder;
    }

    signedS3GetUrl() {
        var params = {
        Bucket: process.env.S3_BUCKET,
        Key: this.folder + this.objectKey
        };
        return S3.getSignedUrl("getObject", params);
    }

    putS3Object(body, ContentType) {// 'image/jpeg' or 'application/pdf'
        var params = {
            Body: body,
            Bucket: process.env.S3_BUCKET,
            ContentType: ContentType,
            ContentDisposition : 'inline',
            Key: this.folder + this.objectKey
        };
        let putPromise = S3.putObject(params).promise();
        return putPromise
    }

    putS3ObjectAndGetSignedUrl(body, ContentType) {// 'image/jpeg' or 'application/pdf'
        // 'image/jpeg' or
        var params = {
            Body: body,
            Bucket: process.env.S3_BUCKET,
            ContentType: ContentType,
            Key: this.folder + this.objectKey
        };
        let putPromise = S3.putObject(params).promise();
        return putPromise.then(res => this.signedS3GetUrl());
  }

  getS3Object() {
    return S3.getObject({
      Bucket: process.env.S3_BUCKET,
      Key: this.folder + this.objectKey,
      ResponseContentType: "application/json"
    }).promise();
  }
}

module.exports = S3Helper;
