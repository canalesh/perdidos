const axios = require("axios");
let cheerio = require("cheerio");
var mexicanStates = require('./mexicanStates')
var databaseHelper = require("./databaseHelper");
var S3Helper = require("./s3Helper");
const fs = require("fs");

var AWS = require("aws-sdk");
AWS.config.update({ region: "us-east-1" });

var comprehend = new AWS.Comprehend({ apiVersion: "2017-11-27" });
const rekognition = new AWS.Rekognition({ apiVersion: "2016-06-27" });
const pdf = require("pdf-parse");
class PerdidosAdmission {
  constructor(statesList, rootSourceURL) {
    this.statesList = statesList;
    this.rootSourceURL = rootSourceURL;
  }

  async registerFacesInCollection (statePerdidos) {
    try {
        var paramsCollection = {
            CollectionId: process.env.FACES_COLLECTION
        };
        await rekognition.createCollection(paramsCollection).promise()
    } catch (err) {
        if (err.code === 'ResourceAlreadyExistsException') {
            console.log(err.message, '... We continue execution...')
        } else {
            console.log('An error ocurred when creating a collection, ', err)
        }
    }
        for (let idx = 0; idx < statePerdidos.length; idx++) {
            try {
                // console.log('Detected Faces condition in index: ', detectedFaces[idx].new)
                let perdidoImageBuffer = await this.downloadFileBuffer(this.rootSourceURL + statePerdidos[idx].image);
                // Here we save it to S3
                let s3Key = `${statePerdidos[idx].id}_${statePerdidos[idx].stateName}.jpg`;
                const s3Helper = new S3Helper(s3Key, "perdidosMedia/");
                let putOperationResult = await s3Helper.putS3Object(perdidoImageBuffer, "image/jpeg");
                statePerdidos[idx].imageS3Key = s3Key;
                var params = {
                    CollectionId: process.env.FACES_COLLECTION,
                    ExternalImageId: statePerdidos[idx].imageS3Key,
                    MaxFaces: 1,
                    DetectionAttributes: [
                    ],
                    Image: {
                        Bytes: perdidoImageBuffer
                    }
                };
                console.log('New face registration with these parameters: ', params)
                let faceId = await rekognition.indexFaces(params).promise() // data.FaceRecords
                // console.log('Here the response from index faces: ', faceId)
                if (faceId.FaceRecords[0]) { 
                    // At least one face result
                    statePerdidos[idx].faceId = faceId.FaceRecords[0].Face.FaceId || undefined;
                    console.log("Registered face: ", statePerdidos[idx].stateName, statePerdidos[idx].faceId);
                } else {
                    statePerdidos[idx].faceId = `NA_${statePerdidos[idx].id}`;
                }
            } catch (error) {
                console.log('An error occurred when indexing faces: ', error)
                statePerdidos[idx].faceId = `NA_${statePerdidos[idx].id}`;
                // return error
                continue
            } 
        }
        return statePerdidos;
       
  }

  async addKeyPhrases (statePerdidos) {
      for (var i = 0; i < statePerdidos.length; i++) {
          try {
              var params = {
                  LanguageCode: 'es', /* required */
                  Text: statePerdidos[i].pdfText.replace(/\n/g, ' ') /* required */
              }
              let keyPhrases = await comprehend.detectKeyPhrases(params).promise()
              if (keyPhrases['KeyPhrases']) {
                  let nonRedundantKeyPhrases = keyPhrases['KeyPhrases'].filter(val => {
                      // if (val.Text.toLowerCase().indexOf(query.toLowerCase()) === -1 && val.Text.split(' ').length > 2 ) {
                      if (val.Text.split(" ").length > 7 && val.Text.indexOf('NACIMIENTO') < 0) {
                          return val;
                      }
                  })
                  // console.log('Here the phrases: ', nonRedundantKeyPhrases)
                  scrapePerdidos[i]['keyPhrases'] = keyPhrases['KeyPhrases'].map(val => val.Text)
                  statePerdidos[i]['keyText'] = nonRedundantKeyPhrases.map(val => val.Text.replace(/\n/g, ' ')).join(" ");
              } else {
                  statePerdidos[i]['keyText'] = 'No se pudo obtener información'
                  scrapePerdidos[i]["keyPhrases"] = []
              }
          } catch (error) {
            console.log('An error occurred when extracting key phrases: ', error)
            continue;
          }
      }
      return statePerdidos
  }

  downloadFileBuffer (url) {
      return axios.request({
          responseType: "arraybuffer",
          url: url,
          method: "get"
      }).then(result => {
          // fs.writeFileSync('test.pdf', result.data);
          const dataBuffer = result.data;
          return dataBuffer
      });
  }

  async addPDFInformation (statePerdidos) {
      try {
          for (var i = 0; i < statePerdidos.length; i++) {
              let pdfFileBuffer = await this.downloadFileBuffer(this.rootSourceURL + "/Alerta/CreaAlertaPDFPublico?numero_reporte=" + statePerdidos[i].id);
              console.log('Got the PDF file for: ', statePerdidos[i].name)
              // fs.writeFileSync(`AlertaAmber_${statePerdidos[i].id}.pdf`, pdfFileBuffer);
              let pdfTextData = await pdf(pdfFileBuffer).then(data => data.text);
              statePerdidos[i].pdfText = pdfTextData
              // Here we save it to S3
              console.log("Sending the PDF file to S3: ", statePerdidos[i].name, statePerdidos[i].pdfText);
              let s3Key = `${statePerdidos[i].id}_${statePerdidos[i].stateName}.pdf`;
              const s3Helper = new S3Helper(s3Key, "perdidosMedia/");
              let putOperationResult = await s3Helper.putS3Object(pdfFileBuffer, "application/pdf");
              statePerdidos[i].pdfS3Key = s3Key;
          }
          return statePerdidos
      } catch (error) {
          console.log('Something happend gettting the PDF: ', error)
          return statePerdidos
      }
  }

  scrapePerdidos(stateIndex) {
    return axios.get(`${this.rootSourceURL}/?id_estado=${stateIndex}`).then(
      response => {
        if (response.status === 200) {
          const html = response.data;
          const $ = cheerio.load(html);
          let statePerdidos = [];
          // console.log(response.data)
          $("img.carousel-image").each(function(ii, elem) {
            statePerdidos[ii] = {
              stateId: stateIndex,
              image: elem.attribs.src,
              id: elem.attribs.src.split('=')[1], // This the Alerta numner
              pdf: elem.parent.attribs.href
            };
            // console.log("Here an element: ", statePerdidos[ii]);
          });
          $(".carousel-caption").each(function(iii, elem) {
            statePerdidos[iii]["info"] = elem.children
              .filter(val => val.name === "ol")
              .map(val => val.children[0].data);
            if (elem.children.filter(val => val.name === "img").length > 0) {
              statePerdidos[iii]["state"] = elem.children
                .filter(val => val.name === "img")
                .map(val => val.attribs.src);
            }
            // console.log("Here an element in carrousel caption: ", elem.children.filter(val => val.name === 'ol').map(val => val.children[0].data))
            // console.log("Here an element in carrousel caption: ", elem.children.filter(val => val.name === "img").map(val => val.attribs.src));
            // console.log("Here an element: ", statePerdidos[iii]);
          });
          // console.log("Here the fresh scraped perdidos: ", statePerdidos);
          return statePerdidos;
        }
      },
      error => {
        console.log('An error happened scraping perdidos: ', error);
        return statePerdidos;
      }
    );
  }

  cleanScrapedData (statePerdidos) {
      for (var i = 0; i < statePerdidos.length; i++) {
          statePerdidos[i]['name'] = statePerdidos[i].info[0] ? statePerdidos[i].info[0] : 'NAN'
          statePerdidos[i]['gender'] = statePerdidos[i].info[1] ? statePerdidos[i].info[1].split(" ")[1] : 'NAN'
          statePerdidos[i]["age"] = statePerdidos[i].info[2] ? statePerdidos[i].info[2].match(/\d+/g)[0] : "NAN";
          statePerdidos[i]['happened'] = statePerdidos[i].info[3] ? statePerdidos[i].info[3].split(" ")[4] : 'NAN'
          if (statePerdidos[i].state) {
              if (statePerdidos[i].state[0].split("/")[3]) {
                  if (statePerdidos[i].state[0].split("/")[3].split(".")[0]) {
                      statePerdidos[i]['resolution'] = statePerdidos[i].state[0].split("/")[3].split(".")[0] ? statePerdidos[i].state[0].split("/")[3].split(".")[0] : 'NAN'
                  }
              }
          } else {
              statePerdidos[i]["resolution"] = 'NAN'
          }
      }
      return statePerdidos
  }

  async addPerdidosToDatabase (statePerdidos) {
      try {
          for (var i = 0; i < statePerdidos.length; i++) {
              let databaseResponse = await databaseHelper.addPerdidoToDatabase(statePerdidos[i]);
              console.log('Here what the database returns: ', databaseResponse)
          }
          return true
      } catch (error) {
        console.log('An error happened when adding perdidos to database', error)
        return false
      }
  }

  async main() {
      try {
          // First I will need to loop between all states
          let allThePerdidos = []
          for (var iii = 0; iii < this.statesList.length; iii++) {
              let stateIndex = this.statesList[iii];
              let stateName = mexicanStates[stateIndex].replace(/ /g, '_');
              // Get the metadata
              console.log('Started scraping: ', stateName)
              let statePerdidos = await this.scrapePerdidos(stateIndex);
              console.log("A total perdidos: ", statePerdidos.length);
              console.log("Started cleaning: ", stateName);
              statePerdidos = this.cleanScrapedData(statePerdidos)
              // Add the state name to the objects
              statePerdidos.map(val => val.stateName = stateName)
              // Here we add all the media information
              console.log("Started adding the PDF: ", stateName);
              statePerdidos = await this.addPDFInformation(statePerdidos);
              console.log("Started adding the KeyPhrases: ", stateName);
              statePerdidos = await this.addKeyPhrases(statePerdidos)
              console.log("The perdidos at this point: ", statePerdidos);
              // Register perdido in a face collection
              console.log("Started adding faces: ", stateName);
              statePerdidos = await this.registerFacesInCollection(statePerdidos)
              statePerdidos.map(val => val.scrapedTime = Date.now())
              // Save the metadata
              console.log("Started saving to Dynamo: ", stateName);
              let databaseResult = await this.addPerdidosToDatabase(statePerdidos)
              allThePerdidos.push(statePerdidos)
              fs.writeFileSync("./downloads/allPerdidos.json", [].concat.apply([], allThePerdidos));
          }
          return [].concat.apply([], allThePerdidos);
      } catch (error) {
        console.log('An error happened when scraping the states', error)
      }
    
  }
}

// var params = {
//   CollectionId: process.env.FACES_COLLECTION
// } 
// rekognition.deleteCollection(params, function(err, data) {
//   if (err) console.log(err, err.stack); // an error occurred
//   else     console.log(data);
// });

// // var params = {
// //   CollectionId: process.env.FACES_COLLECTION
// // } 
// // rekognition.listFaces(params, function(err, data) {
// //   if (err) console.log(err, err.stack); // an error occurred
// //   else     console.log(data);
// // });
// Repetir querétaro (23)
let statesToScrape = [26, 27, 28, 29, 30, 31, 32, 33]
const perdidosAdmission = new PerdidosAdmission(statesToScrape, "http://187.218.233.203");
perdidosAdmission.main()
