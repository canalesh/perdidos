'use strict';
const fs = require("fs");
const parse = require("csv-parse/lib/sync");

const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient();
var table = process.env.DYNAMO_DB;

var comprehend = new AWS.Comprehend({ apiVersion: "2017-11-27" });

function addStatesFromCSV (filePath) {
    function sleep (ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    const contents = fs.readFileSync(filePath, "utf-8");
    // If you made an export of a DynamoDB table you need to remove (S) etc from header
    const data = parse(contents, { columns: true })

    var counter = 0
    data.forEach(async (item) => {
        let dataItems = Object.values(JSON.parse(item.data)).map(val => Object.values(val)[0]);
        // let dateTimeHappened = dataItems[2].split('/')
        item.country = "mexico"
        item.age = dataItems[0]
        item.gender = dataItems[1]
        item.happened = dataItems[2] //  new Date(parseInt(dateTimeHappened[0]), parseInt(dateTimeHappened[1]) - 1, parseInt(dateTimeHappened[2]) - 1)
        item.id = dataItems[3]
        item.imageS3Key = dataItems[4]
        item.keyText = dataItems[5]
        item.name = dataItems[6]
        item.originalImageUrl = dataItems[7]
        item.originalPDFUrl = dataItems[8]
        item.pdfS3Key = dataItems[9]
        item.pdfText = dataItems[10]
        item.resolution = dataItems[11]
        item.scrapedTime = dataItems[12]
        delete item.data
        try {
            var params = {
                LanguageCode: 'es', /* required */
                Text: item.pdfText.replace(/\n/g, ' ') /* required */
            }
            let keyPhrases = await comprehend.detectKeyPhrases(params).promise()
            item.keyPhrases = keyPhrases['KeyPhrases'].map(val => val.Text)
            // console.log("Here an item data: ", item);
            if (!item.keyPhrases) delete item.keyPhrases; //need to remove empty items
            await docClient.put({ TableName: table, Item: item }).promise()
            await sleep(2000);
            counter += 1
            console.log('Done: ', counter, 'of: ', data.lenght)
        } catch (error) {
            if (!item.keyPhrases) delete item.keyPhrases; //need to remove empty items
            await docClient.put({ TableName: table, Item: item }).promise()
            await sleep(2000);
            console.log('An error ocurred adding data to from CSV to DynamoDB', error)
        }      
    })
}

function scanTableForCountry (selectedCountry) {
    var params = {
            TableName: table,
            ProjectionExpression: "#country, id, #perdidoName, age, happened, resolution, imageS3Key, pdfS3Key",
            FilterExpression: "#country = :selectedCountry",
            ExpressionAttributeNames: {
                "#country": "country",
                "#perdidoName": "name"
            },
            // Limit: 10,
            ExpressionAttributeValues: {
                ":selectedCountry": selectedCountry
        }
    };

    console.log("Scanning Perdidos table.");
    return docClient.scan(params).promise();
}

function scanTableForFaceId (faceId) {
    var params = {
        TableName: table,
        ProjectionExpression:"#faceId, id, country, keyText, pdfText, imageS3Key, pdfS3Key, #perdidoName, gender, age, happened",
        FilterExpression: "faceId = :faceId",
        ExpressionAttributeNames: {
            "#faceId": "faceId",
            "#perdidoName": "name"
        },
        ExpressionAttributeValues: {
            ":faceId": faceId
        }
    };

    console.log("Scanning Perdidos table for a faceid.");
    return docClient.scan(params).promise();
}

function addPerdidoToDatabase(perdidoItem) {
    var params = {
        TableName: table,
        Item: {
            "faceId": perdidoItem.faceId,
            "mexicanState": perdidoItem.stateName,
            "id": perdidoItem.id,
            "name": perdidoItem.name,
            "gender": perdidoItem.gender,
            "age": perdidoItem.age,
            "happened": perdidoItem.happened,
            "resolution": perdidoItem.resolution,
            "scrapedTime": perdidoItem.scrapedTime,
            "originalImageUrl": perdidoItem.image,
            "originalPDFUrl": perdidoItem.pdf,
            "pdfText": perdidoItem.pdfText,
            "keyText": perdidoItem.keyText,
            "imageS3Key": perdidoItem.imageS3Key,
            "pdfS3Key": perdidoItem.pdfS3Key,
            "keyPhrases": perdidoItem.keyPhrases
        }
    };
    console.log("Adding a new item to DynamoDB...", params);
    return docClient.put(params).promise()
} 

module.exports.addPerdidoToDatabase = addPerdidoToDatabase;
module.exports.scanTableForCountry = scanTableForCountry;
module.exports.scanTableForFaceId = scanTableForFaceId;

// getStatePerdidos("Coahuila").then(res => {
//   console.log(res);
// });

// scanTableForCountry("Coahuila").then(res => {
//     console.log(res);
// });

// scanTableFaceId("c4222f2e-78d3-415c-a928-2d2b789fc783").then(res => {
//   console.log(res);
// });

// addStatesFromCSV('./downloads/perdidos-sls.csv')
// const contents = fs.readFileSync("./downloads/perdidos-sls.csv", "utf-8");
// // If you made an export of a DynamoDB table you need to remove (S) etc from header
// const data = parse(contents, { columns: true })
// console.log(data)