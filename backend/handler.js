'use strict';
var getURL = require('./helpers/getPostSignedURL');
const dispatch = require("./helpers/lexBot/dispatch"); // Here is for the Lex application
const CheckPerdidoFace = require('./helpers/CheckPerdidoFace')

module.exports.getSignedURL = async (event, context) => {
  // Here first I should get a signedS3 URL to upload the image
  // Then I need to upload the image from the client
  // Then I need to send the file name and perform the face rekognition
  const signedS3Url = await getURL.generatePresignedURL(event.body)
  console.log('Here the created signed url: ', signedS3Url)
  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
    body: JSON.stringify({
      input: event,
      singedS3Url: signedS3Url
    }),
  };
};

module.exports.intents = (event, context, callback) => {
  try {
    console.log(`event.bot.name=${event.bot.name}`);
    dispatch(event).then(response => {
      callback(null, response);
    });
  } catch (err) {
    callback(err);
  }
};

module.exports.checkPerdido = async (event, context) => {
  // Here first I should get a signedS3 URL to upload the image
  // Then I need to upload the image from the client
  // Then I need to send the file name and perform the face rekognition
  console.log("Here the ingested event data: ", event);
  console.log("Here the ingested context data: ", context);
  const s3Config = {
    imageName: event.body
  };
  const checkPerdidoFace = new CheckPerdidoFace(s3Config);
  const checkResponse = await checkPerdidoFace.main();
  console.log("Here the created signed url: ", checkResponse);
  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
    body: JSON.stringify({
      reception: checkResponse
    })
  };
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
