// Using require() in ES5
const axios = require('axios')

const fbAppId = process.env.FB_APP_ID
const fbAppSecret = process.env.FB_APP_SECRET;
const limit = 1300
const alertaAmberAlbum = "487511084716853";
let allImages = [];

function getImagesDatarecursive (sourceUrl, limit, data = []) {
  return axios.get(sourceUrl).then(response => {
      if (!response.data.paging || !response.data.paging.next || [].concat.apply([], data).length >= limit) return data;
        data.push(response.data.data);
        return getImagesDatarecursive(response.data.paging.next, limit, data);
    });
}

const getImagesData = async (clientId, clientSecret, albumId, limit) => {
    try {
        if (limit < 101) {
            let dataResults = [];
            let results = await axios.get(`https://graph.facebook.com/v2.8/${albumId}/photos?fields=images,id,name,created_time&limit=${limit}&access_token=${clientId}|${clientSecret}`);
            console.log('Here the results pagination: ', results.data.paging)
            dataResults.push(results.data.data)
            return [].concat.apply([], dataResults);
        } else {
            dataResults = await getImagesDatarecursive(`https://graph.facebook.com/v2.8/${albumId}/photos?fields=images,id,name,created_time&limit=${limit}&access_token=${clientId}|${clientSecret}`, limit);
            return [].concat.apply([], dataResults);
        }
        // console.log("Here the data results: ", [].concat.apply([], dataResults));
        
    } catch (error) {
        console.error(error)
    }
}

const fbImagesRequest = async () => {
    try {
        const res = await getImagesData(fbAppId, fbAppSecret, alertaAmberAlbum, limit);
        // console.log('Here the res:', res.data.data)
        // let data = JSON.parse(res.data)
        res.map(val => {
            let mainImage = val.images[0] ? val.images[0].source : null
            allImages.push({
                id: val.id,
                text: val.name,
                image: mainImage,
                created_time: val.created_time
            });
        });
        let validImages = allImages.filter(val => {
            if (val.text) {
                if (val.text.indexOf("continúa") >= 0 || val.text.indexOf("localizado") >= 0 || val.text.indexOf("localizada") >= 0) {
                    return false;
                } else if (val.text.indexOf("búsqueda") >= 0) {
                    return true;
                }
                return;
            } else {
                return false;
            }
        })
        console.log("Here all the images: ", validImages, validImages.length, allImages.length);
    } catch (error) {
        console.log(error);
    }
    
}

fbImagesRequest()
