const fs = require('fs');
const pdf = require('pdf-parse');
const axios = require("axios");


var AWS = require("aws-sdk");
AWS.config.update({ region: "us-east-1" });

var comprehend = new AWS.Comprehend({ apiVersion: "2017-11-27" });
const rekognition = new AWS.Rekognition({ apiVersion: "2016-06-27" });

// let dataBuffer = fs.readFileSync('./CreaAlertaPDFPublico.pdf');

var allPerdidosJSON = fs.readFileSync('./allPerdidos_1.json','utf8');
var allPerdidos = JSON.parse(allPerdidosJSON);
// console.log('Here all perdidos: ', allPerdidos)

let alertaAmberPDF = "http://187.218.233.203/Alerta/CreaAlertaPDFPublico?numero_reporte=581";
let alertaAmberImage = "http://187.218.233.203/Alerta/ObtenerFotoDesaparecido?numero_reporte=581";

// Here we get the text information from the PDF
axios.request({
        responseType: "arraybuffer",
        url: alertaAmberPDF,
        method: "get"
    }).then(result => {
        // fs.writeFileSync('test.pdf', result.data);
        const dataBuffer = result.data;
        pdf(dataBuffer).then(function (data) {
            console.log('Well, now gettting the text... ')
            // PDF text
            var params = {
                LanguageCode: 'es', /* required */
                Text: data.text /* required */
            }
            console.log(data.text);

            // let keyPhrases = await comprehend.detectKeyPhrases(params).promise()
            comprehend.detectKeyPhrases(params, (error, res) => { // 
                // let keyPhrases = res["Entities"].map(val => val.Text); 
                // console.log("Here the keyphrase: ", error, res);
                let nonRedundantKeyPhrases = res['KeyPhrases'].filter(val => {
                    // if (val.Text.toLowerCase().indexOf(query.toLowerCase()) === -1 && val.Text.split(' ').length > 2 ) {
                    if (val.Text.split(" ").length > 7 && val.Text.indexOf('NACIMIENTO') < 0) {
                      return val;
                    }
                })
                console.log('Here the phrases: ', nonRedundantKeyPhrases)
            }
            );

        });
    });

// Here we get the face information from the image
axios.request({
    responseType: "arraybuffer",
    url: alertaAmberImage,
    method: "get"
}).then(result => {
    var params = {
        Image: {
            Bytes: result.data
        },
        Attributes: [
            "ALL",
        ]
    };
    // Here I get the face details (interested in the AgeRange)
    rekognition.detectFaces(params, (error, res) => {
        console.log('Here the face results: ', res.FaceDetails, res.FaceDetails[0].Emotions)
    })// data.FaceDetails
})

