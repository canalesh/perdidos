const axios = require("axios");
let cheerio = require("cheerio");
var fs = require("fs");

function scrapePerdidos (i) {
    return axios.get(`http://187.218.233.203/?id_estado=${i}`).then(
        response => {
            if (response.status === 200) {
                const html = response.data;
                const $ = cheerio.load(html);
                let statePerdidos = []
                // console.log(response.data)
                $("img.carousel-image").each(function (ii, elem) {
                    statePerdidos[ii] = { image: elem.attribs.src, pdf: elem.parent.attribs.href };
                    // console.log("Here an element: ", statePerdidos[ii]);
                });
                $(".carousel-caption").each(function (iii, elem) {
                    statePerdidos[iii]['info'] = elem.children.filter(val => val.name === 'ol').map(val => val.children[0].data)
                    if (elem.children.filter(val => val.name === "img").length > 0) {
                        statePerdidos[iii]['state'] = elem.children.filter(val => val.name === "img").map(val => val.attribs.src)
                    }
                    // console.log("Here an element in carrousel caption: ", elem.children.filter(val => val.name === 'ol').map(val => val.children[0].data))
                    // console.log("Here an element in carrousel caption: ", elem.children.filter(val => val.name === "img").map(val => val.attribs.src));
                    console.log("Here an element: ", statePerdidos[iii]);
                });
                return statePerdidos;
            }
        },
        error => console.log(error)
    );
}

let allPerdidos = [];
const getAllPerdidos = async () => {
    for (var i = 2; i <= 33; i++) {
        let statePerdidos = await scrapePerdidos(i);
        allPerdidos.push(statePerdidos)
        console.log('State done: ', i)
    }
    fs.writeFile(`allPerdidos_1.json`, JSON.stringify([].concat.apply([], allPerdidos)));
    console.log("All perdidos written...");
}

// Here the scraping template:
// {/* <div class="carousel-feature" style="border-color:#DB070B">
//     <a href="/Alerta/CreaAlertaPDFPublico?numero_reporte=657" target="_blank">
//         <img class="carousel-image" alt="Image Caption" style="border-color:#DB070B" src="/Alerta/ObtenerFotoDesaparecido?numero_reporte=657">
//     </a>

//         <div class="carousel-caption">
//             <img src="../../Images/Desactivada.png">
//                 <ol class="labelNombreDesaparecido">PATRICIO RONQUILLO TORRES</ol>
//                 <ol>G&eacute;nero: Masculino</ol>
//                 <ol>
//                     Edad: 6 a&#241;os                                </ol>
//                 <ol>Fecha de los hechos: 09/06/2016</ol>
//     </div>
// </div> */}

getAllPerdidos();
